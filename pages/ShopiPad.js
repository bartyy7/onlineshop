import React from "react";
import ItemCard from "../components/ItemCard";
import { iPaditem1 } from "../data";

const ShopIpad = () => {
  return (
    <>
      <section className="py-4 container">
        <div className="row justify-content-center">
          {iPaditem1.product.map((item, index) => {
            return (
              <div className="shop-page">
                <ItemCard
                  img={item.img}
                  img3={item.img3}
                  img2={item.img2}
                  img4={item.img4}
                  adapter={item.adapter}
                  info={item.info}
                  cable={item.cable}
                  price={item.price}
                  title={item.title}
                  item={item}
                  key={index}
                />
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
};
export default ShopIpad;
