import React from "react";
import "./pages.css";
import Mac from "../images/macbookpage.png";
import { Link } from "react-router-dom";

export default function PageMacTop() {
  return (
    <div className="MacBook-page pages-container">
      <div className="pages-text">
        <div>
          <h1>MacBook Pro</h1>
          <p>
            The Apple M1 chip gives the 13.inch MacBook Pro speed and power
            beyond belief.
          </p>
          <spam className="price">
            <p>From €1999</p>
          </spam>

          <div className="page-buttons">
            <button>
              <Link to="/mac/buy-macbook-pro-13">Buy</Link>
            </button>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <img src={Mac} />
    </div>
  );
}
