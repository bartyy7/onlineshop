import React from "react";
import ItemCard from "../components/ItemCard";
import { iphoneitem1 } from "../data";
import "./shoppage.css";

const ShopiPhone = () => {
  return (
    <>
      <section className="py-4 container">
        <div className="row justify-content-center">
          {iphoneitem1.product.map((item, index) => {
            return (
              <div className="shop-page">
                <ItemCard
                  img={item.img}
                  img3={item.img3}
                  img2={item.img2}
                  info={item.info}
                  cable={item.cable}
                  price={item.price}
                  title={item.title}
                  item={item}
                  key={index}
                />
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
};
export default ShopiPhone;
