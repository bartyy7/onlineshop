import React from "react";
import Navbar from "../components/Navbar";
import StoreFirst from "./StoreFirst";
import StoreSecond from "../pages/StoreSecond";
import StoreThird from "../pages/StoreThird";
import Footer from "../components/Footer";
import InfoIcon from "@mui/icons-material/Info";
import StoreIcon from "@mui/icons-material/Store";
import ShoppingBagIcon from "@mui/icons-material/ShoppingBag";

function Store() {
  return (
    <main className="home phone">
      <Navbar
        firstIcon={ShoppingBagIcon}
        secondIcon={InfoIcon}
        thirdIcon={StoreIcon}
      />

      <section id="first">
        <StoreFirst />
      </section>
      <section id="second">
        <StoreSecond />
      </section>
      <section id="third">
        <StoreThird />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
}

export default Store;
