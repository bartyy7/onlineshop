import React from "react";
import "./pages.css";
import Watch from "../images/applewatch2.png";
import { Link } from "react-router-dom";

export default function PageWatchTop() {
  return (
    <div className="watch-page pages-container ">
      <div className="pages-text">
        <div>
          <h1>Apple Watch</h1>
          <p>Full screen ahead.</p>
          <spam className="price">
            <p>From €399</p>
          </spam>
          <div className="page-buttons button-color">
            <button>
              <Link to="/applewatch/buy-watch7">Buy</Link>
            </button>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <img src={Watch} />
    </div>
  );
}
