import React from "react";
import "./pages.css";
import iPad from "../images/ipadpage5.png";
import { Link } from "react-router-dom";

export default function PageMacTop() {
  return (
    <div className="iPad-page pages-container">
      <div className="pages-text Mac-color">
        <div>
          <h1>iPad Pro</h1>
          <p>The ultimate iPad experience.</p>
          <spam className="price">
            <p>From €999</p>
          </spam>

          <div className="page-buttons">
            <button>
              {" "}
              <Link to="/ipad/buy-ipad-pro">Buy</Link>
            </button>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <img src={iPad} />
    </div>
  );
}
