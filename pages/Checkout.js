import React, { useState } from "react";
import "./checkout.css";
import { useCart } from "react-use-cart";
import { Link } from "react-router-dom";
import ModalAppleCare from "../components/ModalAppleCare";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";

const Checkout = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    address: "",
    postcode: "",
    city: "",
    mountlypay: false,
    fullpay: true,
    applecare: false,
    phone: "",
  });

  const {
    isEmpty,
    items,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();
  const buy = () => {
    alert("Thank you for your order");
  };
  const removedDecimal = cartTotal * 0.8;
  const dph = Math.round(removedDecimal);
  const extraapple = cartTotal + 200;

  const handleChange = (event) => {
    const { name, value, type, checked } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  function handleSubmit(event) {
    event.preventDefault();
    console.log(formData);
    console.log(`Mountly pay: ${formData.mountlypay}`);
    console.log(`Full pay: ${formData.fullpay}`);
    buy();
    emptyCart();
    setHotovo((hotovo) => !hotovo);
  }

  function mountlypay() {
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        mountlypay: true,
        fullpay: false,
      };
    });
  }
  function fullpay() {
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        mountlypay: false,
        fullpay: true,
      };
    });
  }
  const [hotovo, setHotovo] = useState(false);

  return (
    <div className="checkout">
      {hotovo && (
        <div className="checkout-thankyou">
          <p>Thank You !</p>
          <Link to="/">Come back to home page</Link>
        </div>
      )}

      {!hotovo && (
        <div className="checkout-form">
          <h2>Shipping address</h2>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <input
                required
                type="text"
                placeholder="First Name"
                onChange={handleChange}
                name="firstName"
                value={formData.firstName}
              />

              <input
                required
                type="text"
                placeholder="Last Name"
                onChange={handleChange}
                name="lastName"
                value={formData.lastName}
              />
            </div>

            <input
              required
              type="text"
              placeholder="Address"
              onChange={handleChange}
              name="address"
              value={formData.address}
            />

            <div className="form-row">
              <input
                required
                type="number"
                placeholder="Postcode"
                onChange={handleChange}
                name="postcode"
                value={formData.postcode}
              />

              <input
                required
                type="text"
                placeholder="City"
                onChange={handleChange}
                name="city"
                value={formData.city}
              />
            </div>

            <input
              type="tel"
              placeholder="Phone Number"
              onChange={handleChange}
              name="phone"
              value={formData.phone}
            />

            <input
              required
              type="email"
              placeholder="Email"
              onChange={handleChange}
              name="email"
              value={formData.email}
            />

            <div className="form-applecare">
              <input
                id="applecare"
                type="checkbox"
                name="applecare"
                onChange={handleChange}
                checked={formData.applecare}
              />
              <div className="applecare-label">
                <label htmlFor="applecare">
                  Apple Care+ ( 199€ or 9.99/mo. )
                </label>
                <p>
                  <ModalAppleCare />
                </p>
              </div>
            </div>

            <div className="pay-type-container">
              <div
                value={formData.fullpay}
                onClick={fullpay}
                className="pay-type"
                style={{
                  outline: formData.fullpay
                    ? "2px solid #0071e3"
                    : "1px solid grey",
                }}
              >
                <p>Full pay</p>
                <p>One-time payment</p>

                <p>
                  Total price : {formData.applecare ? extraapple : cartTotal}
                </p>
              </div>
              <div
                value={formData.mountlypay}
                onClick={mountlypay}
                className="pay-type"
                style={{
                  outline: formData.mountlypay
                    ? "2px solid #0071e3"
                    : "1px solid grey",
                }}
              >
                <p>Mountly payment</p>

                <p>
                  {formData.applecare
                    ? (extraapple / 24).toFixed(2)
                    : (cartTotal / 24).toFixed(2)}
                  /mountly for 24 mo
                </p>

                <p>
                  Total price : {formData.applecare ? extraapple : cartTotal}
                </p>
              </div>
            </div>

            <button>Submit</button>
          </form>
        </div>
      )}
      {!hotovo && (
        <div className="checkout-allitems">
          <div>
            <h2>Summary</h2>
            <Link to="/basket">
              Back to basket <ShoppingBagOutlinedIcon sx={{ fontSize: 15 }} />
            </Link>
          </div>

          <table className="checkout-table-container">
            {items.map((item, index) => {
              return (
                <tr className="checkout-table" key={index}>
                  <td
                    className="checkout-table-img"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <img src={item.img} />
                  </td>

                  <td
                    className="checkout-table-title"
                    style={{
                      fontWeight: "bolder",
                    }}
                  >
                    {item.title}
                  </td>
                  <td>({item.quantity})</td>
                  <td style={{ textAlign: "right" }}>{item.price}€</td>
                </tr>
              );
            })}
          </table>

          <div className="checkout-price-container">
            <div className="checkout-price">
              <div className="checkout-price-item">
                <p>Subtotal:</p>
                <p>{formData.applecare ? extraapple : cartTotal}€</p>
              </div>
              <div className="checkout-price-item">
                <p>Without DPH :</p>
                <p>{dph}€</p>
              </div>
              <div className="checkout-price-item checkout-price-item-border">
                <p>Shipping:</p>
                <p>FREE</p>
              </div>
              <div className="checkout-price-item">
                <h2>Total:</h2>
                <h2>{formData.applecare ? extraapple : cartTotal}€</h2>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default Checkout;
