import React from "react";
import Navbar from "../components/Navbar";
import PageiPhoneTop from "./PageiPhoneTop";
import PageiPadTop from "./PageiPadTop";
import PageMacTop from "./PageMacTop";
import PageWatchTop from "./PageWatchTop";
import Footer from "../components/Footer";
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import TabletMacIcon from "@mui/icons-material/TabletMac";
import WatchIcon from "@mui/icons-material/Watch";
import LaptopMacIcon from "@mui/icons-material/LaptopMac";

const PageHome = () => {
  return (
    <main className="home">
      <Navbar
        firstIcon={PhoneIphoneIcon}
        secondIcon={TabletMacIcon}
        thirdIcon={LaptopMacIcon}
        forthIcon={WatchIcon}
      />
      <section id="first">
        <PageiPhoneTop />
      </section>

      <section id="second">
        <PageiPadTop />
      </section>
      <section id="third">
        <PageMacTop />
      </section>
      <section id="forth">
        <PageWatchTop />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
};

export default PageHome;
