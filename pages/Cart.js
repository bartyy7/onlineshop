import React from "react";
import { useCart } from "react-use-cart";
import { Link } from "react-router-dom";
import "./cart.css";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

const Cart = () => {
  const {
    isEmpty,
    items,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  const removedDecimal = cartTotal * 0.8;
  const dph = Math.round(removedDecimal);

  if (isEmpty)
    return (
      <h1 className="basket-container basket-empty">Your cart is empty</h1>
    );
  return (
    <section className="basket-container">
      <h1>Review your bag.</h1>
      <p>Free delivery and free returns.</p>

      <table className="basket-table-container">
        <tbody>
          {items.map((item, index) => {
            return (
              <tr className="basket-table" key={index}>
                <td style={{ display: "flex", justifyContent: "center" }}>
                  <img src={item.img} />
                </td>

                <td style={{ fontWeight: "bolder" }}>{item.title}</td>

                <td>{item.price}€</td>

                <td className="quantity-deskop">Quantity: ({item.quantity})</td>

                <td className="basket-table-buttons">
                  <td className="quantity-phone">({item.quantity})</td>
                  <button
                    onClick={() =>
                      updateItemQuantity(item.id, item.quantity - 1)
                    }
                    className="btn btn-info ms-2"
                  >
                    <RemoveCircleOutlineIcon
                      sx={{
                        fontSize: 45,
                        color: "#525252",
                        "@media (max-width:1339px)": {
                          fontSize: 35,
                        },
                      }}
                    />
                  </button>
                  <button
                    onClick={() =>
                      updateItemQuantity(item.id, item.quantity + 1)
                    }
                    className="btn btn-info ms-2"
                  >
                    <AddCircleOutlineIcon
                      sx={{
                        fontSize: 45,
                        color: "#525252",
                        "@media (max-width:1339px)": {
                          fontSize: 35,
                        },
                      }}
                    />
                  </button>
                  <button
                    onClick={() => removeItem(item.id)}
                    className="btn btn-danger ms-2"
                  >
                    <DeleteOutlineIcon
                      sx={{
                        fontSize: 45,
                        color: "#525252",
                        "@media (max-width:1339px)": {
                          fontSize: 35,
                        },
                      }}
                    />
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="basket-price-container">
        <div className="basket-price">
          <div className="basket-price-item">
            <p>Subtotal:</p>
            <p>{cartTotal}€</p>
          </div>
          <div className="basket-price-item">
            <p>Without DPH :</p>
            <p>{dph}€</p>
          </div>
          <div className="basket-price-item basket-price-item-border">
            <p>Shipping:</p>
            <p>FREE</p>
          </div>
          <div className="basket-price-item">
            <h2>Total:</h2>
            <h2>{cartTotal}€</h2>
          </div>
          <div className="basket-price-item-buttons">
            <button onClick={() => emptyCart()}>Clear Cart</button>
            <Link to="checkout">Checkout</Link>
          </div>
        </div>
      </div>

      <div></div>
    </section>
  );
};

export default Cart;
