import React from "react";
import PageMacTop from "./PageMacTop";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import DevicesMac from "../components/DevicesMac";
import InfoMac from "../components/InfoMac";
import DevicesOtherIcon from "@mui/icons-material/DevicesOther";
import LaptopMacIcon from "@mui/icons-material/LaptopMac";
import SmartDisplayIcon from "@mui/icons-material/SmartDisplay";

const PageMac = () => {
  return (
    <main className="home phone">
      <Navbar
        firstIcon={LaptopMacIcon}
        secondIcon={DevicesOtherIcon}
        thirdIcon={SmartDisplayIcon}
      />
      <section id="first">
        <PageMacTop />
      </section>
      <section id="second">
        <DevicesMac type="Mac" />
      </section>
      <section id="third">
        <InfoMac />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
};

export default PageMac;
