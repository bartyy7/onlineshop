import React from "react";
import "./pages.css";
import iPhone from "../images/iphonepage.png";
import { Link } from "react-router-dom";

export default function PageiPhoneTop() {
  return (
    <div className="iPhone-page-main pages-container">
      <div className="pages-text Mac-color">
        <div>
          <h1>iPhone 13 Pro</h1>
          <p>Your new superpower.</p>
          <spam className="price">
            <p>From €999</p>
          </spam>

          <div className="page-buttons button-color">
            <button>
              <Link to="/iphone/buy-iphone-13-pro">Buy</Link>
            </button>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <img src={iPhone} />
    </div>
  );
}
