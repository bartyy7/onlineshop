import React from "react";
import "./storepages.css";
import Modal from "../components/ModalSpecialist";
import Modal2 from "../components/ModalTips";
import Modal3 from "../components/ModalRepairs";
import Modal4 from "../components/ModalActivities";

export default function StoreSecond() {
  return (
    <div className="store-info page-temp">
      <div className="store-container ">
        <p>
          <spam style={{ color: " #1d1d1d" }}>Help is here. </spam> Whenever and
          however you need it.
        </p>

        <div className="store-img">
          <Modal className="modal-img" />
          <div className="store-info-right">
            <Modal2 />
            <Modal3 />
          </div>
          <Modal4 />
        </div>

        <div className="store-img-phone">
          <div className="store-info-right2">
            <Modal />
            <Modal4 />
          </div>
          <div className="store-info-right">
            <Modal2 />
            <Modal3 />
          </div>
        </div>
      </div>
    </div>
  );
}
