import React from "react";
import Navbar from "../components/Navbar";
import PageiPhoneTop from "./PageiPhoneTop";
import Footer from "../components/Footer";
import DevicesiPhone from "../components/DevicesiPhone";
import InfoiPhone from "../components/InfoiPhone";
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import SmartDisplayIcon from "@mui/icons-material/SmartDisplay";
import DevicesOtherIcon from "@mui/icons-material/DevicesOther";

export default function PageiPhone() {
  return (
    <main className="home phone">
      <Navbar
        firstIcon={PhoneIphoneIcon}
        secondIcon={DevicesOtherIcon}
        thirdIcon={SmartDisplayIcon}
      />
      <section id="first">
        <PageiPhoneTop />
      </section>
      <section id="second">
        <DevicesiPhone type="iPhone" />
      </section>
      <section id="third">
        <InfoiPhone />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
}
