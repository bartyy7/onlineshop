import React from "react";
import "./storepages.css";
import Stores from "../components/Stores";
import Store1 from "../images/store1.jpg";
import Store2 from "../images/store2.jpg";
import Store3 from "../images/store3.jpg";

export default function StoreThird() {
  return (
    <div className="stores-page page-temp">
      <h1>Stores in Slovakia</h1>
      <div className="stores-row">
        <Stores
          img={Store1}
          name="Apple OC Central"
          address="Pribinova, Šumavská 8, 821 08 Ružinov Bratislava "
          openTime="10:00"
        />
        <Stores
          img={Store2}
          name="Apple Bory Mall"
          address="Lamač 6780, 841 03 Bratislava"
          openTime="10:00"
        />
        <Stores
          img={Store3}
          name="Apple OC Laugaricio"
          address="7271 Belá, 911 01 Trenčín"
          openTime="10:00"
        />
      </div>
    </div>
  );
}
