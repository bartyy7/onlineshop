import React from "react";
import PageiPadTop from "./PageiPadTop";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import DevicesiPad from "../components/DevicesiPad";
import InfoiPad from "../components/InfoiPad";
import DevicesOtherIcon from "@mui/icons-material/DevicesOther";
import SmartDisplayIcon from "@mui/icons-material/SmartDisplay";
import TabletMacIcon from "@mui/icons-material/TabletMac";

export default function PageiPad() {
  return (
    <main className="home phone">
      <Navbar
        firstIcon={TabletMacIcon}
        secondIcon={DevicesOtherIcon}
        thirdIcon={SmartDisplayIcon}
      />
      <section id="first">
        <PageiPadTop />
      </section>
      <section id="second">
        <DevicesiPad type="iPad" />
      </section>
      <section id="third">
        <InfoiPad />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
}
