import React from "react";
import ItemCard from "../components/ItemCard";
import { Macitem2 } from "../data";

const ShopMac = () => {
  return (
    <>
      <section className="py-4 container">
        <div className="row justify-content-center">
          {Macitem2.product.map((item, index) => {
            return (
              <div className="shop-page">
                <ItemCard
                  link={item.link}
                  img={item.img}
                  img3={item.img3}
                  img2={item.img2}
                  img4={item.img4}
                  info={item.info}
                  adapter={item.adapter}
                  cable={item.cable}
                  price={item.price}
                  title={item.title}
                  item={item}
                  key={index}
                />
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
};
export default ShopMac;
