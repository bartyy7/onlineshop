import React from "react";
import "./storepages.css";
import { Link } from "react-router-dom";
import Mac from "../images/iconmac.png";
import iPhone from "../images/iconiphone.png";
import Ipad from "../images/iconipad.png";
import Watch from "../images/iconwatch.png";
import AppleStore from "../images/applestorebackground7.png";
import AppleStorePhone from "../images/applestorebackground6.png";

export default function StoreFirst() {
  return (
    <div className="store-page-first page-temp">
      <div className="store-left-site">
        <div className="store-page-first-text">
          <h1>
            <spam style={{ color: " #1d1d1d" }}>Store. </spam>The best way to
            buy the products you love.
          </h1>
        </div>
        <div className="store-page-first-container">
          <div className="store-page-first-items">
            <Link to="">
              <img className="iPad-image" src={Mac} />
              <p>Mac</p>
            </Link>
          </div>
          <div className="store-page-first-items">
            <Link to="">
              <img className="iPad-image" src={iPhone} />
              <p>iPhone</p>
            </Link>
          </div>
          <div className="store-page-first-items">
            <Link to="">
              <img className="iPad-image" src={Ipad} />
              <p>iPad</p>
            </Link>
          </div>
          <div className="store-page-first-items">
            <Link to="">
              <img className="iPad-image" src={Watch} />
              <p>Apple Watch</p>
            </Link>
          </div>
        </div>
      </div>
      <div className="store-right-site">
        <img className="store-img" src={AppleStore} />
        <img className="store-phone-img" src={AppleStorePhone} />
      </div>
    </div>
  );
}
