import React from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import PageWatchTop from "./PageWatchTop";
import DevicesWatch from "../components/DevicesWatch";
import InfoWatch from "../components/InfoWatch";
import WatchIcon from "@mui/icons-material/Watch";
import SmartDisplayIcon from "@mui/icons-material/SmartDisplay";
import DevicesOtherIcon from "@mui/icons-material/DevicesOther";

export default function PageWatch() {
  return (
    <main className="home phone">
      <Navbar
        firstIcon={WatchIcon}
        secondIcon={DevicesOtherIcon}
        thirdIcon={SmartDisplayIcon}
      />
      <section id="first">
        <PageWatchTop />
      </section>
      <section id="second">
        <DevicesWatch type="Apple Watch" />
      </section>
      <section id="third">
        <InfoWatch />
      </section>
      <section>
        <Footer />
      </section>
    </main>
  );
}
