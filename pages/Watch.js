import React from "react";
import AppleWatch from "../images/watch.png";

export default function Watch() {
  return (
    <div className="Watch-page">
      <div className="Watch-text">
        <h1>Your family joined at the wrist.</h1>
        <p>
          Now family members who don't have an iPhone can stay in touch with
          Apple Watch.
        </p>
        <div className="iPhone-buttons">
          <button>Learn more</button>
          <button>Shop</button>
        </div>
      </div>
      <img src={AppleWatch} />
    </div>
  );
}
