import watch1 from "./images/shopwatchbuy.png";
import watch2 from "./images/shopwatchbuy2.png";
import watch3 from "./images/shopwatchbuy3.png";
import iPad1 from "./images/shopipadimg.png";
import iPad2 from "./images/shopipadimg2.png";
import iPad3 from "./images/shopipadimg3.png";
import iphone1 from "./images/shoppagephone.png";
import iphone2 from "./images/shoppagephone2.png";
import iphone3 from "./images/shoppagephone3.png";
import Mac1 from "./images/shopmacbook.png";
import Mac2 from "./images/shopmacbook2.png";
import Mac3 from "./images/shopmacbook3.png";
import cable from "./images/shopcable.png";
import cablemac from "./images/shopcablemac.jpg";
import cableipad from "./images/shopcableipad.png";
import cablewatch from "./images/shopcablewatch.jpg";
import cablewatch2 from "./images/shopcablewatch2.jpg";
import cablewatch3 from "./images/shopcablewatch3.jpg";
import phone from "./images/shopphone.png";
import phone2 from "./images/shopphone2.png";
import phone3 from "./images/shopphone3.png";
import shopwatch from "./images/shopwatch.jpg";
import shopmac from "./images/shopmac.png";
import shopipadair from "./images/shopipad.jpg";
import shopipadpro from "./images/shopipadpro.png";
import shopipad from "./images/shopipad2.jpg";
import adaptermac from "./images/shopadaptermac.jpg";
import shopadapteripad from "./images/shopadapteripad.png";
import adapterwatch from "./images/shopadapterwatch.jpg";

const Macitem1 = {
  product: [
    {
      id: 10,
      link: "/",
      title: "MacBook Air",
      price: 999,
      img: Mac1,
      img3: cablemac,
      img2: shopmac,
      img4: adaptermac,
      adapter: "30W USB-C Power Adapter",
      cable: "USB-C Charge Cable (2 m)",
      info: "Apple M1 chip with 8‑core CPU, 7‑core GPU, and 16‑core Neural Engine 8GB unified memory ,256GB SSD storage Retina display with True Tone,Backlit Magic Keyboard - US English,Touch ID, Force Touch trackpad, Two Thunderbolt / USB 4 ports",
    },
  ],
};
const Macitem2 = {
  product: [
    {
      id: 11,
      link: "/",
      title: "MacBook Pro 13”",
      price: 1299,
      img: Mac2,
      img3: cablemac,
      img2: shopmac,
      img4: adaptermac,
      adapter: "30W USB-C Power Adapter",
      cable: "USB-C Charge Cable (2 m)",
      info: "Apple M1 chip with 8‑core CPU, 8‑core GPU, and 16‑core Neural Engine, 8GB unified memory 256GB SSD storage, 13-inch Retina display with True Tone, Backlit Magic Keyboard - US English, Touch Bar and Touch ID, Two Thunderbolt / USB 4 ports",
    },
  ],
};
const Macitem3 = {
  product: [
    {
      id: 12,
      link: "/",
      title: "MacBook Pro 14”,16”",
      price: 1999,
      img: Mac3,
      img3: cablemac,
      img2: shopmac,
      img4: adaptermac,
      adapter: "30W USB-C Power Adapter",
      cable: "USB-C Charge Cable (2 m)",
      info: "Apple M1 Pro with 8-core CPU, 14-core GPU, 16-core Neural Engine, 16GB unified memory, 512GB SSD storage, 67W USB-C Power Adapter, 14-inch Liquid Retina XDR display, Three Thunderbolt 4 ports, HDMI port, SDXC card slot, MagSafe 3 port, Backlit Magic Keyboard with Touch ID - US English",
    },
  ],
};

const iphoneitem1 = {
  product: [
    {
      id: 7,
      title: "iPhone 13 Pro",
      price: 1269,
      img: iphone1,
      img3: cable,
      img2: phone,
      cable: "USB-C Charge Cable",
      info: "A dramatically more powerful camera system. A display so responsive, every interaction feels new again. The world’s fastest smartphone chip. Exceptional durability. And a huge leap in battery life.Let’s Pro.",
    },
  ],
};
const iphoneitem2 = {
  product: [
    {
      id: 8,
      title: "iPhone 13",
      price: 957,
      img: iphone2,
      img3: cable,
      img2: phone2,
      cable: "USB-C Charge Cable",
      info: "Our most advanced dual‑camera system ever.Durability that’s front and center. And edge to edge.A lightning-fast chip that speeds up everything you do.",
    },
  ],
};
const iphoneitem3 = {
  product: [
    {
      id: 9,
      title: "iPhone SE",
      price: 689,
      img: iphone3,
      img3: cable,
      img2: phone3,
      cable: "USB-C Charge Cable",
      info: "At the heart of iPhone SE you’ll find the same superpowerful A15 Bionic chip that’s in iPhone 13.",
    },
  ],
};

const iPaditem1 = {
  product: [
    {
      id: 4,
      title: "iPad Pro",
      price: 799,
      img: iPad1,
      img3: cableipad,
      img2: shopipadpro,
      img4: shopadapteripad,
      adapter: "20W USB-C Power Adapter",
      cable: "USB-C Charge Cable",
      info: "The ultimate iPad experience. Now with breakthrough M1 performance, a breathtaking XDR display, and blazing‑fast 5G wireless. Buckle up.",
    },
  ],
};
const iPaditem2 = {
  product: [
    {
      id: 5,
      title: "iPad Air",
      price: 599,
      img: iPad2,
      img3: cableipad,
      img2: shopipadair,
      img4: shopadapteripad,
      adapter: "20W USB-C Power Adapter",
      cable: "USB-C Charge Cable",
      info: "Supercharged by the Apple M1 chip. 12MP Ultra Wide front camera, with Center Stage. Blazing-fast 5G. Works with Apple Pencil and Magic Keyboard. Five gorgeous colors.",
    },
  ],
};
const iPaditem3 = {
  product: [
    {
      id: 6,
      title: "iPad",
      price: 329,
      img: iPad3,
      img3: cable,
      img2: shopipad,
      img4: shopadapteripad,
      adapter: "20W USB-C Power Adapter",
      cable: "USB-C Charge Cable",
      info: "Powerful. Easy to use. Versatile. The new iPad is designed for all the things you love to do. Work, play, create, learn, stay connected, and more. All at an incredible value.",
    },
  ],
};

const watchitem1 = {
  product: [
    {
      id: 1,
      title: "Apple Watch Series 7",
      price: 399,
      img: watch1,
      img3: cablewatch,
      img2: shopwatch,
      img4: adapterwatch,
      adapter: "30W USB-C Power Adapter",
      cable: "USB-C Charge Cable (2 m)",
      info: "The aluminum case is lightweight and made from 100 percent recycled aerospace-grade alloy. The Sport Band is made from a durable yet surprisingly soft high-performance fluoroelastomer with an innovative pin-and-tuck closure.",
    },
  ],
};
const watchitem2 = {
  product: [
    {
      id: 2,
      title: "Apple Watch SE",
      price: 279,
      img: watch2,
      img3: cablewatch3,
      img2: shopwatch,
      img4: adapterwatch,
      adapter: "20W USB-C Power Adapter",
      cable: "USB-C Charge Cable",
      info: "The aluminum case is lightweight and made from 100 percent recycled aerospace-grade alloy. The Solo Loop is made from soft, stretchable silicone and designed for ultracomfort with no clasps or buckles.",
    },
  ],
};
const watchitem3 = {
  product: [
    {
      id: 3,
      title: "Apple Watch Series 3",
      price: 199,
      img: watch3,
      img3: cablewatch2,
      img2: shopwatch,
      img4: adapterwatch,
      adapter: "20W USB-C Power Adapter",
      cable: "USB-C Charge Cable",
      info: "38mm or 42mm case Second-generation Retina OLED display, 1000 nits, Ion-X glass display, GPS model, S3 SiP with dual-core processor; W2 wireless chip, Digital Crown, Optical heart sensor, High and low heart rate notifications and irregular heart rhythm notification1, Emergency SOS2, Water resistant 50 meters3, Wi-Fi and Bluetooth 4.2, GPS/GNSS and barometric altimeter, Built-in speaker and mic, 8GB capacity",
    },
  ],
};

export { watchitem1 };
export { watchitem2 };
export { watchitem3 };
export { iPaditem1 };
export { iPaditem2 };
export { iPaditem3 };
export { iphoneitem1 };
export { iphoneitem2 };
export { iphoneitem3 };
export { Macitem1 };
export { Macitem2 };
export { Macitem3 };
