import React from "react";
import Header from "./components/Header";
import PageHome from "./pages/PageHome";
import Store from "./pages/Store";
import { Routes, Route } from "react-router-dom";
import PageMac from "./pages/PageMac";
import PageiPad from "./pages/PageiPad";
import PageiPhone from "./pages/PageiPhone";
import PageWatch from "./pages/PageWatch";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import ShopWatch from "./pages/ShopWatch";
import ShopWatch2 from "./pages/ShopWatch2";
import ShopWatch3 from "./pages/ShopWatch3";
import { CartProvider } from "react-use-cart";
import ShopiPhone from "./pages/ShopiPhone";
import ShopiPhone2 from "./pages/ShopiPhone2";
import ShopiPhone3 from "./pages/ShopiPhone3";
import ShopiPad from "./pages/ShopiPad";
import ShopiPad2 from "./pages/ShopiPad2";
import ShopiPad3 from "./pages/ShopiPad3";
import ShopMac from "./pages/ShopMac";
import ShopMac2 from "./pages/ShopMac2";
import ShopMac3 from "./pages/ShopMac3";

const App = () => {
  return (
    <div>
      <CartProvider>
        <Header />
        <Routes>
          <Route exact path="/" element={<PageHome />} />
          <Route exact path="/store" element={<Store />} />
          <Route exact path="/mac" element={<PageMac />} />
          <Route exact path="/ipad" element={<PageiPad />} />
          <Route exact path="/iphone" element={<PageiPhone />} />
          <Route exact path="/applewatch" element={<PageWatch />} />
          <Route exact path="/applewatch/buy-watch7" element={<ShopWatch />} />
          <Route
            exact
            path="/applewatch/buy-watchse"
            element={<ShopWatch2 />}
          />
          <Route exact path="/applewatch/buy-watch3" element={<ShopWatch3 />} />
          <Route
            exact
            path="/iphone/buy-iphone-13-pro"
            element={<ShopiPhone />}
          />
          <Route exact path="/iphone/buy-iphone-13" element={<ShopiPhone2 />} />
          <Route exact path="/iphone/buy-iphone-se" element={<ShopiPhone3 />} />
          <Route exact path="/ipad/buy-ipad-pro" element={<ShopiPad />} />
          <Route exact path="/ipad/buy-ipad-air" element={<ShopiPad2 />} />
          <Route exact path="/ipad/buy-ipad" element={<ShopiPad3 />} />
          <Route exact path="/mac/buy-macbook-air" element={<ShopMac />} />
          <Route exact path="/mac/buy-macbook-pro-13" element={<ShopMac2 />} />
          <Route
            exact
            path="/mac/buy-macbook-pro-14-16"
            element={<ShopMac3 />}
          />
          <Route exact path="/basket" element={<Cart />} />
          <Route exact path="/basket/checkout" element={<Checkout />} />
        </Routes>
      </CartProvider>
    </div>
  );
};

export default App;
