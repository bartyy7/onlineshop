import React from "react";
import "./devicesContainer.css";
import iphone1 from "../images/iphone1.png";
import iphone2 from "../images/iphone2.png";
import iphone3 from "../images/iphone3.png";
import Chip from "../images/deviceChip3.png";
import Chip2 from "../images/deviceChip4.png";
import Touch from "../images/devicesTouch.png";
import FaceID from "../images/devicesTouch2.png";
import Camera from "../images/devices5G.png";
import Camera1 from "../images/devicesCamera5.png";
import Camera2 from "../images/devicesCamera6.png";
import Camera3 from "../images/devicesCamera7.png";
import Camera4 from "../images/devicesCamera8.png";
import Battery from "../images/devicesBattery.png";

import DevicesItem from "./DevicesItem";

export default function Devices(props) {
  return (
    <div className="devices">
      <div className="devices-container">
        <h1>Which {props.type} is right for you?</h1>
        <div className="devices-items">
          <DevicesItem
            buyitem="/iphone/buy-iphone-13-pro"
            alpine={true}
            white={true}
            beige={true}
            black={true}
            blue={true}
            img={iphone1}
            name="iPhone 13 Pro"
            info="The ultimate iPhone."
            price="999"
            imgChip={Chip}
            chip="A15 Bionic chip"
            core="6.7″ or 6.1”"
            core2="All-screen OLED display2"
            imgMemory={Camera1}
            Memory="Pro camera system Telephoto, Wide, Ultra Wide"
            imgBattery={Battery}
            Battery="Up to 28 hours video playback4"
            imgCamera={Camera}
            Camera="Superfast 5G cellular3"
            imgTouch={FaceID}
            Touch="Face ID"
          />
          <DevicesItem
            buyitem="/iphone/buy-iphone-13"
            alpine={true}
            white={true}
            pink={true}
            black={true}
            blue={true}
            red={true}
            img={iphone2}
            name="iPhone 13"
            info="A total powerhouse."
            price="699"
            imgChip={Chip}
            chip="A15 Bionic chip"
            core="6.1″ or 5.4″"
            core2="All-screen OLED display2"
            imgMemory={Camera2}
            Memory="Advanced dual-camera system Wide, Ultra Wide"
            imgBattery={Battery}
            Battery="Up to 19 hours video playback4"
            imgCamera={Camera}
            Camera="Superfast 5G cellular3"
            imgTouch={FaceID}
            Touch="Face ID"
          />
          <DevicesItem
            buyitem="/iphone/buy-iphone-se"
            white={true}
            red={true}
            black={true}
            img={iphone3}
            name="iPhone SE"
            info="Serious power, value."
            price="429"
            imgChip={Chip2}
            chip="A15 Bionic chip"
            core="4.7″"
            core2="LCD display"
            imgMemory={Camera3}
            Memory="Single-camera system Wide"
            imgBattery={Battery}
            Battery="Up to 15 hours video playback4"
            imgCamera={Camera}
            Camera="5G cellular3"
            imgTouch={Touch}
            Touch="Touch ID"
          />
        </div>
      </div>
    </div>
  );
}
