import React from "react";
import "./infoComponent.css";
import InfoComponent from "./InfoComponent";
import InfoMac1 from "../images/infomac1.png";
import InfoMac2 from "../images/infomac2.png";

export default function InfoMac() {
  return (
    <div className="devices">
      <div className="info-container">
        <InfoComponent
          header="All your devices. One seamless experience."
          color="#fafafa"
          info=""
          img={InfoMac1}
        />
        <InfoComponent
          header="All your devices. One seamless experience."
          color="#fafafa"
          info=""
          img={InfoMac2}
        />
      </div>
    </div>
  );
}
