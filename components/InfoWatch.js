import React from "react";
import "./infoComponent.css";
import InfoComponent from "./InfoComponent";
import InfoWatch1 from "../images/infowatch1.png";
import InfoWatch2 from "../images/infowatch2.png";

export default function InfoWatch() {
  return (
    <div className="devices">
      <div className="info-container">
        <InfoComponent
          header="It's the ultimate device for a healthy life."
          color="#f5f5f7"
          info=""
          img={InfoWatch1}
        />
        <InfoComponent
          header="Apple Music."
          color="#f5f5f7"
          info="Over 90 million songs.
            Start listening for free today."
          img={InfoWatch2}
        />
      </div>
    </div>
  );
}
