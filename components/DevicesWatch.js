import React from "react";
import "./devicesContainer.css";
import watch1 from "../images/watch1.png";
import watch2 from "../images/watch2.png";
import watch3 from "../images/watch3.png";
import WatchDevices1 from "../images/deviceswatch1.png";
import WatchDevices2 from "../images/deviceswatch2.png";
import WatchDevices3 from "../images/deviceswatch3.png";
import SOS from "../images/devicesos.png";
import ECG from "../images/deviceecg.png";
import Hearth from "../images/devicehearth.png";
import Circle from "../images/devicecircle.png";
import Line from "../images/deviceline.png";

import DevicesItem from "./DevicesItem";

export default function Devices(props) {
  return (
    <div className="devices">
      <div className="devices-container">
        <h1>Which {props.type} is right for you?</h1>
        <div className="devices-items watch-items">
          <DevicesItem
            buyitem="/applewatch/buy-watch7"
            pink={true}
            black={true}
            blue={true}
            purple={true}
            navyblue={true}
            dark={true}
            green={true}
            grey={true}
            img={watch1}
            name="Apple Watch Series 7"
            price="399"
            imgChip={WatchDevices1}
            chip="Apple M1 chip"
            core="45mm or 41mm"
            core2="Swimproof1"
            imgMemory={Circle}
            Memory="Blood Oxygen app2"
            imgBattery={ECG}
            Battery="ECG app3"
            imgCamera={Hearth}
            Camera="High and low heart
            rate notifications  
            Irregular heart
            rhythm notification4"
            imgTouch={SOS}
            Touch="Emergency SOS
            International
            emergency calling
            Fall detection"
          />
          <DevicesItem
            buyitem="/applewatch/buy-watchse"
            white={true}
            grey={true}
            beige={true}
            img={watch2}
            name="Apple Watch SE"
            price="279"
            imgChip={WatchDevices2}
            chip="Apple M1 chip"
            core="44mm or 40mm"
            core2="Swimproof1"
            imgMemory={Line}
            imgBattery={Line}
            imgCamera={Hearth}
            Camera="High and low heart
            rate notifications
            Irregular heart
            rhythm notification4"
            imgTouch={SOS}
            Touch="Emergency SOS
            International
            emergency calling
            Fall detection"
          />
          <DevicesItem
            buyitem="/applewatch/buy-watch3"
            white={true}
            grey={true}
            img={watch3}
            name="Apple Watch Series 3"
            price="199"
            imgChip={WatchDevices3}
            chip="Apple M1 Pro chip or Apple M1 Max chip"
            core="42mm or 38mm"
            core2="Swimproof1"
            imgMemory={Line}
            imgBattery={Line}
            imgCamera={Hearth}
            Camera="High and low heart
            rate notifications
            Irregular heart
            rhythm notification4"
            imgTouch={SOS}
            Touch="Emergency SOS"
          />
        </div>
      </div>
    </div>
  );
}
