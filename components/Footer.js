import React from "react";
import "./footer.css";
import AppleIcon from "@mui/icons-material/Apple";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

export default function Footer() {
  return (
    <div className="footer">
      <a href="#top">
        <KeyboardArrowUpIcon
          sx={{
            fontSize: 70,
            "@media (max-width:1339px)": {
              fontSize: 30,
            },
          }}
        />
      </a>

      <p>
        <AppleIcon /> Created by Barty
      </p>
    </div>
  );
}
