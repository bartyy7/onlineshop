import React, { useState } from "react";
import "./navbar.css";

import AppleIcon from "@mui/icons-material/Apple";

import { Link } from "react-router-dom";

export default function Navbar(props) {
  const [active, setActive] = useState(true);
  const [active2, setActive2] = useState(false);
  const [active3, setActive3] = useState(false);
  const [active4, setActive4] = useState(false);

  const activeColor = () => {
    setActive((active) => !active);
    setActive2(false);
    setActive3(false);
    setActive4(false);
  };

  const activeColor2 = () => {
    setActive2((active) => !active);
    setActive(false);
    setActive3(false);
    setActive4(false);
  };

  const activeColor3 = () => {
    setActive3((active) => !active);
    setActive2(false);
    setActive(false);
    setActive4(false);
  };
  const activeColor4 = () => {
    setActive4((active) => !active);
    setActive3(false);
    setActive2(false);
    setActive(false);
  };

  return (
    <div className="side-navbar">
      <p>
        <Link to="/">
          <AppleIcon
            sx={{
              position: "absolute",
              top: 20,
              left: 20,
              fontSize: 50,
              color: "#1d1d1d",
            }}
          />
        </Link>
      </p>

      {props.firstIcon && (
        <a href="#first" onClick={activeColor}>
          <props.firstIcon
            sx={{
              fontSize: 40,
              "@media (max-width:1339px)": {
                fontSize: 30,
              },
              color: active ? "#ffffff" : "#acacac",
            }}
          />
        </a>
      )}
      {props.secondIcon && (
        <a href="#second" onClick={activeColor2}>
          <props.secondIcon
            sx={{
              fontSize: 40,
              "@media (max-width:1339px)": {
                fontSize: 30,
              },
              color: active2 ? "#ffffff" : "#acacac",
            }}
          />
        </a>
      )}
      {props.thirdIcon && (
        <a href="#third" onClick={activeColor3}>
          <props.thirdIcon
            sx={{
              fontSize: 40,
              "@media (max-width:1339px)": {
                fontSize: 30,
              },
              color: active3 ? "#ffffff" : "#acacac",
            }}
          />
        </a>
      )}
      {props.forthIcon && (
        <a href="#forth" onClick={activeColor4}>
          <props.forthIcon
            sx={{
              fontSize: 40,
              "@media (max-width:1339px)": {
                fontSize: 30,
              },
              color: active4 ? "#ffffff" : "#acacac",
            }}
          />
        </a>
      )}
    </div>
  );
}
