import React from "react";
import "./devicesContainer.css";
import Mac1 from "../images/mac1.png";
import Mac2 from "../images/mac2.png";
import Mac3 from "../images/mac3.png";
import Chip from "../images/devicesChip.png";
import Touch from "../images/devicesTouch.png";
import Camera from "../images/devicesCamera.png";
import Memory from "../images/devicesMemory.png";
import Battery from "../images/devicesBattery.png";

import DevicesItem from "./DevicesItem";

export default function Devices(props) {
  return (
    <div className="devices">
      <div className="devices-container">
        <h1>Which {props.type} is right for you?</h1>
        <div className="devices-items">
          <DevicesItem
            buyitem="/mac/buy-macbook-air"
            grey={true}
            white={true}
            beige={true}
            img={Mac1}
            name="MacBook Air"
            price="999"
            imgChip={Chip}
            chip="Apple M1 chip"
            core="8-core"
            core2="CPU"
            imgMemory={Memory}
            Memory="Up to 16GB unified memory"
            storage="2TB"
            storage2="Maximum configurable storage1"
            display="13.3”"
            display2="Retina display2"
            imgBattery={Battery}
            Battery="Up to 18 hours battery life"
            imgCamera={Camera}
            Camera="720p FaceTime HD camera"
            Weight="2.8 lb"
            Weight2="Weight"
            imgTouch={Touch}
            Touch="Touch ID"
          />
          <DevicesItem
            buyitem="/mac/buy-macbook-pro-13"
            grey={true}
            white={true}
            img={Mac2}
            name="MacBook Pro 13”"
            price="1299"
            imgChip={Chip}
            chip="Apple M1 chip"
            core="8-core"
            core2="CPU"
            imgMemory={Memory}
            Memory="Up to 16GB unified memory"
            storage="2TB"
            storage2="Maximum configurable storage1"
            display="13.3”"
            display2="Retina display2"
            imgBattery={Battery}
            Battery="Up to 20 hours battery life"
            imgCamera={Camera}
            Camera="720p FaceTime HD camera"
            Weight="3.0 lb."
            Weight2="Weight"
            imgTouch={Touch}
            Touch="Touch Bar and Touch ID"
          />
          <DevicesItem
            buyitem="/mac/buy-macbook-pro-14-16"
            grey={true}
            white={true}
            img={Mac3}
            name="MacBook Pro 14”,16”"
            price="1999"
            imgChip={Chip}
            imgChip2={Chip}
            chip="Apple M1 Pro chip or Apple M1 Max chip"
            core="8-core"
            core2="CPU"
            imgMemory={Memory}
            Memory="Up to 16GB unified memory"
            storage="8TB"
            storage2="Maximum configurable storage1"
            display="14.2” or 16.2”"
            display2="Liquid Retina XDR display2"
            imgBattery={Battery}
            Battery="Up to 21 hours battery life"
            imgCamera={Camera}
            Camera="720p FaceTime HD camera"
            Weight="3.5 lb. or 4.7 lb."
            Weight2="Weight"
            imgTouch={Touch}
            Touch="Touch ID"
          />
        </div>
      </div>
    </div>
  );
}
