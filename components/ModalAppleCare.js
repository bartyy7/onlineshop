import * as React from "react";
import "./modal.css";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import InfoIcon from "@mui/icons-material/Info";

export default function BasicModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <InfoIcon
        onClick={handleOpen}
        sx={{ color: "#7a7a7a", cursor: "pointer" }}
      />

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="modal-text modal-box"
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            borderRadius: 5,
            bgcolor: "background.paper",
            border: "0px solid #000",
            outline: "none",
            p: 4,
          }}
        >
          <Typography
            className="modal-text"
            id="modal-modal-title"
            variant="h3"
            component="h2"
            sx={{ paddingBottom: 1 }}
          >
            <h3>
              Get service and support from the people who know iPhone best.
            </h3>
          </Typography>
          <Typography
            id="modal-modal-description"
            className="modal-text"
            sx={{
              mt: 2,
              color: "#1d1d1d;",
              paddingTop: 0,
              paddingBottom: 2,
            }}
          >
            <p>
              With AppleCare+, you get accidental damage protection, extended
              hardware coverage, and 24/7 priority technical support.
            </p>
            <h2>AppleCare+ Coverage Options</h2>
            <p>
              Every iPhone comes with one year of hardware repair coverage
              through its limited warranty(Opens in a new window) and up to 90
              days of complimentary support(Opens in a new window). Get
              AppleCare+ to extend that coverage and receive additional features
              such as 24/7 priority tech support and accidental damage
              protection.
            </p>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
}
