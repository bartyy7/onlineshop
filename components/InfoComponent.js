import React from "react";
import "./infoComponent.css";

export default function InfoComponent(props) {
  return (
    <div className="info-item" style={{ backgroundColor: `${props.color}` }}>
      <div className="info-text">
        <h1>{props.header}</h1>
        <p>{props.info}</p>
        <div className="item-page-buttons">
          <button>Learn more</button>
        </div>
      </div>
      <img src={props.img} />
    </div>
  );
}
