import * as React from "react";
import "./modal.css";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import InfoGirl from "../images/infogirl.jpg";

export default function BasicModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <img onClick={handleOpen} className="modal-image" src={InfoGirl} />

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="modal-text modal-box"
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            borderRadius: 5,
            bgcolor: "background.paper",
            border: "0px solid #000",
            outline: "none",
            p: 4,
          }}
        >
          <Typography
            className="modal-text"
            id="modal-modal-title"
            variant="h3"
            component="h2"
            sx={{ paddingBottom: 1 }}
          >
            <h3> Apple Specialists are here for you.</h3>
          </Typography>
          <Typography
            id="modal-modal-description"
            className="modal-text"
            sx={{
              mt: 2,
              color: "#1d1d1d;",
              paddingTop: 0,
              paddingBottom: 2,
            }}
          >
            <p>
              Need help finding what’s right for you? Shop with a Specialist
              online. Or reserve a shopping session at an Apple Store. American
              Sign Language interpreters are also available in stores and online
              through SignTime, an on-demand video service.
            </p>
          </Typography>
          <Typography
            className="modal-text"
            sx={{
              paddingTop: 0,
            }}
          >
            <div className="modal-anchor">
              <a href="">
                Shop with a Specialist online(Opens in a new window)
              </a>
              <a href="">Contact us in American Sign Language</a>
              <a href="">Reserve an in-store shopping session</a>
            </div>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
}
