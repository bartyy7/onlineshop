import React, { useState } from "react";
import "./header.css";
import { Link } from "react-router-dom";
import AppleIcon from "@mui/icons-material/Apple";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import DragHandleIcon from "@mui/icons-material/DragHandle";
import CloseIcon from "@mui/icons-material/Close";
import { useCart } from "react-use-cart";

const Header = () => {
  const { totalItems } = useCart();
  const [navBar, setNavBar] = useState(true);

  const toggleNavBar = () => {
    setNavBar((navBar) => !navBar);
  };
  return (
    <div className="top-header">
      <div className="header">
        <div className="header-left">
          <p>
            <Link to="">
              <AppleIcon sx={{ fontSize: 45 }} />
            </Link>
          </p>

          <p className="header-phone" onClick={toggleNavBar}>
            {navBar ? (
              <DragHandleIcon sx={{ fontSize: 45, color: "#acacac" }} />
            ) : (
              <CloseIcon sx={{ fontSize: 45, color: "#acacac" }} />
            )}
          </p>
        </div>
        {!navBar && (
          <ul class="phone-menu">
            <li>
              <Link onClick={toggleNavBar} to="store">
                Store
              </Link>
            </li>
            <li>
              <Link onClick={toggleNavBar} to="mac">
                Mac
              </Link>
            </li>
            <li>
              <Link onClick={toggleNavBar} to="ipad">
                iPad
              </Link>
            </li>
            <li>
              <Link onClick={toggleNavBar} to="iphone">
                iPhone
              </Link>
            </li>
            <li>
              <Link onClick={toggleNavBar} to="applewatch">
                Watch
              </Link>
            </li>
            <li>
              <Link onClick={toggleNavBar} to="basket">
                Basket
              </Link>
            </li>
          </ul>
        )}

        <div className="menu">
          <p>
            <Link to="store">Store</Link>
          </p>
          <p>
            <Link to="mac">Mac</Link>
          </p>
          <p>
            <Link to="ipad">iPad</Link>
          </p>
          <p>
            <Link to="iphone">iPhone</Link>
          </p>
          <p>
            <Link to="applewatch">Watch</Link>
          </p>

          <p>
            <Link to="basket">
              <ShoppingBagOutlinedIcon />
              {totalItems}
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Header;
