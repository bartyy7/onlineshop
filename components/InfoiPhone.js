import React from "react";
import "./infoComponent.css";
import InfoComponent from "./InfoComponent";
import InfoiPhone1 from "../images/infoiphone1.png";
import InfoiPhone2 from "../images/infoiphone2.png";

export default function InfoiPhone() {
  return (
    <div style={{ backgroundColor: "#fafafa" }} className="devices">
      <div className="info-container">
        <InfoComponent
          header="Switching to iPhone is super simple."
          color="#ffffff"
          info=""
          img={InfoiPhone1}
        />
        <InfoComponent
          header="Apple Music"
          color="#ffffff"
          info="Over 90 million songs. Start listening for free today."
          img={InfoiPhone2}
        />
      </div>
    </div>
  );
}
