import React from "react";
import "./infoComponent.css";
import InfoComponent from "./InfoComponent";
import InfoiPad1 from "../images/infoipad1.png";
import InfoiPad2 from "../images/infoipad2.png";

export default function InfoiPad() {
  return (
    <div className="devices">
      <div className="info-container">
        <InfoComponent
          header="Find the right Apple Pencil for your iPad."
          img={InfoiPad2}
        />
        <InfoComponent
          header="iCloud"
          info="The best place for all your photos, files, and more."
          img={InfoiPad1}
        />
      </div>
    </div>
  );
}
