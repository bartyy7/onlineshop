import * as React from "react";
import "./modal.css";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import InfoIcons from "../images/infoicons.jpg";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function BasicModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <img
        onClick={handleOpen}
        className="modal-image store-info-right-img "
        src={InfoIcons}
      />
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="modal-text modal-box"
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            borderRadius: 5,
            bgcolor: "background.paper",
            border: "0px solid #000",
            outline: "none",
            p: 4,
          }}
        >
          <Typography
            className="modal-text"
            id="modal-modal-title"
            variant="h3"
            component="h2"
            sx={{ paddingBottom: 1 }}
          >
            <h3> We cover everything from the basics to top tips.</h3>
          </Typography>
          <Typography
            className="modal-text"
            id="modal-modal-description"
            sx={{
              mt: 2,
              color: "#1d1d1d;",
              paddingTop: 0,
              paddingBottom: 2,
            }}
          >
            <p>
              Buy a new device from Apple and we’ll help you get the most out of
              it with a free one-on-one Personal Session.Once your online
              purchase ships, you’ll receive an invitation to schedule your
              online session. When you purchase or pick up in a store, a
              Specialist will go over your new device with you then.
            </p>
          </Typography>
          <Typography
            className="modal-text"
            sx={{
              paddingTop: 0,
            }}
          >
            <div className="modal-anchor">
              <a href="">Learn more</a>
            </div>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
}
