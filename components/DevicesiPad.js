import React from "react";
import "./devicesContainer.css";
import iPad1 from "../images/iPad1.png";
import iPad2 from "../images/iPad2.png";
import iPad3 from "../images/iPad3.png";
import Chip from "../images/devicesChip.png";
import Chip2 from "../images/devicesChip2.png";
import G5 from "../images/devices5G.png";
import G4 from "../images/devices4G.png";
import Battery1 from "../images/devicesBattery1.png";
import Battery2 from "../images/devicesBattery2.png";
import Battery3 from "../images/devicesBattery3.png";
import Camera1 from "../images/devicesCamera1.png";
import Camera2 from "../images/devicesCamera2.png";
import Camera3 from "../images/devicesCamera3.png";
import Camera4 from "../images/devicesCamera4.png";
import DevicesItem from "./DevicesItem";

export default function Devices(props) {
  return (
    <div className="devices">
      <div className="devices-container">
        <h1>Which {props.type} is right for you?</h1>
        <div className="devices-items">
          <DevicesItem
            buyitem="/ipad/buy-ipad-pro"
            dark={true}
            white={true}
            img={iPad1}
            name="iPad Pro"
            info="The ultimate iPad experience."
            price="799"
            imgChip={Chip}
            chip="M1 chip"
            core="8-core"
            core2="CPU"
            imgMemory={Camera1}
            Memory="12MP Wide and 10MP
            Ultra Wide back cameras"
            storage="12MP"
            storage2="TrueDepth front camera with Center Stage"
            display="12.9” and 11””"
            display2="12.9” Liquid Retina XDR display1"
            imgBattery={Battery1}
            Battery="USB-C connector with support for Thunderbolt / USB 4"
            imgCamera={G5}
            Camera="5G cellular2"
          />
          <DevicesItem
            buyitem="/ipad/buy-ipad-air"
            dark={true}
            white={true}
            beige={true}
            blue={true}
            green={true}
            img={iPad2}
            name="iPad Air"
            info="Powerful. Colorful. Wonderful."
            price="599"
            imgChip={Chip}
            chip="M1 chip"
            core="8-core"
            core2="CPU"
            imgMemory={Camera2}
            Memory="12MP Wide back camera"
            storage="12MP"
            storage2="Ultra Wide front camera with Center Stage"
            display="13.3”"
            display2="Retina display2"
            imgBattery={Battery2}
            Battery="USB-C connector"
            imgCamera={G5}
            Camera="5G cellular2"
          />
          <DevicesItem
            buyitem="/ipad/buy-ipad"
            dark={true}
            white={true}
            img={iPad3}
            name="iPad”"
            info="Surprisingly affordable."
            price="329"
            imgChip={Chip2}
            chip="A13 Bionic chip"
            core="8-core"
            core2="CPU"
            imgMemory={Camera3}
            Memory="8MP Wide back camera"
            storage="12MP"
            storage2="Ultra Wide front camera with Center Stage"
            display="13.3”"
            display2="Retina display2"
            imgBattery={Battery3}
            Battery="Up to 20 hours battery life"
            imgCamera={G4}
            Camera="4G cellular2"
          />
        </div>
      </div>
    </div>
  );
}
