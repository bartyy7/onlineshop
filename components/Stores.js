import React from "react";
import "./stores.css";

const Stores = (props) => {
  return (
    <div className="store-cart">
      <img src={props.img} />
      <div className="store-cart-text">
        <div className="store-cart-text-item">
          <h2>{props.name}</h2>
          <p>{props.address}</p>
        </div>
        <p>Opens at {props.openTime} a.m.</p>
      </div>
    </div>
  );
};
export default Stores;
