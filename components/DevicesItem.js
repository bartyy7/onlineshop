import React from "react";
import "./deviceItem.css";
import { Link } from "react-router-dom";

export default function DevicesItem(props) {
  return (
    <div className="device-item-container">
      <div className="device-item device-item-watch">
        <img src={props.img} />
        <div className="devices-colors">
          {props.purple && <span className="dot dot11">{props.purple}</span>}
          {props.alpine && <span className="dot dot7">{props.alpine}</span>}
          {props.pink && <span className="dot dot8">{props.pink}</span>}
          {props.navyblue && <span className="dot dot9">{props.navyblue}</span>}
          {props.dark && <span className="dot dot4">{props.dark}</span>}
          {props.black && <span className="dot dot10">{props.black}</span>}
          {props.grey && <span className="dot dot1">{props.grey}</span>}
          {props.white && <span className="dot dot2">{props.white}</span>}
          {props.beige && <span className="dot dot3">{props.beige}</span>}
          {props.blue && <span className="dot dot5">{props.blue}</span>}
          {props.green && <span className="dot dot6">{props.green}</span>}
          {props.red && <span className="dot dot12">{props.red}</span>}
        </div>
        <h2>{props.name}</h2>
        {props.info && <p className="item-info-text">{props.info}</p>}
        <p>From €{props.price}</p>
        <div className="item-page-buttons">
          <div className="page-buttons2 button-color">
            <button>
              <Link
                to={{
                  pathname: props.buyitem,
                }}
              >
                Buy
              </Link>
            </button>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <div className="device-list">
        <div className="device-list-item">
          <img src={props.imgChip} />
          <img src={props.imgChip2} />
          <p>{props.chip}</p>
        </div>
        <div className="device-list-item">
          <p className="device-bold">{props.core}</p>
          <p className="device-list-item">{props.core2}</p>
        </div>
        <div className="device-list-item">
          <img src={props.imgMemory} />
          <p>{props.Memory}</p>
        </div>
        {props.storage && (
          <div className="device-list-item">
            <p className="device-bold">{props.storage}</p>
            <p>{props.storage2}</p>
          </div>
        )}
        {props.display && (
          <div className="device-list-item">
            <p className="device-bold">{props.display}</p>
            <p>{props.display2}</p>
          </div>
        )}

        <div className="device-list-item">
          <img src={props.imgBattery} />
          <p>{props.Battery}</p>
        </div>
        <div className="device-list-item">
          <img src={props.imgCamera} />
          <p>{props.Camera}</p>
        </div>
        {props.Weight && (
          <div className="device-list-item">
            <p className="device-bold">{props.Weight}</p>
            <p>{props.Weight2}</p>
          </div>
        )}

        {props.imgTouch && (
          <div className="device-list-item">
            <img src={props.imgTouch} />
            <p>{props.Touch}</p>
          </div>
        )}
      </div>
    </div>
  );
}
