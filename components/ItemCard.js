import React, { useState } from "react";
import { useCart } from "react-use-cart";
import "./itemcard.css";

const ItemsCard = (props) => {
  const { addItem } = useCart();

  return (
    <div>
      <div className="shop-item-card">
        <div className="shop-item-img">
          <img src={props.img} className="" alt={props.title} />
        </div>
        <div className="shop-item-text">
          <h2 className="">Buy {props.title}</h2>
          <p>{props.info}</p>
          <div className="shop-item-text-button">
            <p className="">{props.price}€</p>
            <button className="" onClick={() => addItem(props.item)}>
              Add To Cart
            </button>
          </div>
        </div>
      </div>
      <div className="shop-item-box-container">
        <h2>What’s in the Box</h2>
        <div className="shop-item-box">
          {props.img2 && (
            <div className="shop-item-box-img ">
              <img src={props.img2} className="" alt={props.title} />
              <p>{props.title}</p>
            </div>
          )}
          {props.img3 && (
            <div className="shop-item-box-img">
              <img src={props.img3} className="" alt={props.title} />
              <p>{props.cable}</p>
            </div>
          )}
          {props.img4 && (
            <div className="shop-item-box-img">
              <img src={props.img4} className="" alt={props.title} />
              <p>{props.adapter}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ItemsCard;
